#%%
import requests
import csv
import pandas as pd
import datetime as dt
from calendar import monthrange
import itertools
import numpy as np
import os
#%%
def get_earthquakes(year=2018,month=11,date=1):
    startdate = dt.date(year,month,date)
    startdatestr = startdate.strftime('%Y-%m-%d')
    # enddate = dt.date(year,month,monthrange(year,month)[1])
    enddate = dt.datetime.now()
    url = 'http://earthquake.usgs.gov/fdsnws/event/1/query'
    query = {
        'format': 'csv',
        'starttime':startdatestr,
        'endtime':enddate.strftime('%Y-%m-%d'),
        'latitude': '61.391',
        'longitude': '-150.025',
        'maxradiuskm': '20',
        'minmagnitude': '0'
    }
    response = requests.get(url, params=query)
    lines = (line.decode('utf-8') for line in response.iter_lines())
    csvreader = csv.DictReader(lines)
    with open('earthquake-data.csv','w') as outfile:
        fields = ['time','latitude','longitude','depth','mag','type', 'horizontalError', 'dmin', 'net', 'updated', 'depthError', 'status', 'magNst', 'rms', 'nst', 'place', 'id', 'locationSource', 'magSource', 'magType', 'gap', 'magError']
        writer = csv.DictWriter(outfile,fields,delimiter=',')
        writer.writeheader()
        for row in csvreader:
            writer.writerow(row)
    return writer

if __name__ =='__main__':
    get_earthquakes()

#%%
get_earthquakes()

#%%
