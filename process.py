#%%
#need to process the csv from USGS so that I can read it into pandas without issues
with open('earthquake-data.csv','r') as quake_in, open('earthquake-parsed.csv','w') as quake_out:
    for line in quake_in:
        # new_line = line[0:10]+' '+line[11:23]
        # new_line = ' '.join([line[0:10],line[11,23],line[24:]])
        if 'time,latitude' not in line:
            new_line = line.replace('T',' ',2).replace('Z','',2)
        else:
            new_line = line
        quake_out.write(new_line)
#%%