#%%
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import math
import numpy as np
#%%
# https://earthquake.usgs.gov/earthquakes/search/

#import the data from USGS which was saved as a CSV
quake = pd.read_csv('earthquake-parsed.csv')
quake['time'] = pd.to_datetime(quake['time'],yearfirst=True)
quake['time'] = quake['time'] - pd.Timedelta(9,'h')
start = pd.Timestamp(2018,11,30,8,29)
quake['time_delta'] = (quake['time'] - start)/pd.Timedelta(1,'d')
quake['mag_rounded'] = round(quake['mag']*2)/2
#%%
#plot the frequency of large and small aftershocks as a stacked histogram and a scatter plot over time
mag_cutoff = 2.75
day_lmp = 1
quake_plot = quake[quake['time_delta']>-100000]
quake_plot['color'] = np.where(quake_plot['mag']>mag_cutoff,'maroon','blue')
quake_plot['time_delta_rounded'] = round(quake_plot['time_delta']/day_lmp)*day_lmp

x = [quake_plot[quake_plot['mag']>mag_cutoff]['time_delta'],quake_plot[quake_plot['mag']<mag_cutoff]['time_delta']]
fig,(ax1,ax2,ax3) = plt.subplots(3,1,figsize=(20,10),gridspec_kw={'height_ratios':[1,2,2]})
for ax in (ax1,ax2,ax3):
        ax.grid(0.3)
ax1.hist(x,
        bins=27,
        stacked=True,
        label=['> magnitude '+str(mag_cutoff),'< magnitude '+str(mag_cutoff)],
        color=['maroon','blue'],alpha=0.6)
ax1.legend(loc='best')
ax1.set(xlabel='Hours Since Main Event',ylabel='Number of Earthquakes')
ax2.scatter(quake_plot['time_delta'],quake_plot['mag'],color=quake_plot['color'],alpha=0.2)
ax2.set(xlabel='Hours Since Main Event',ylabel='Magnitude of Earthquakes')
sns.violinplot(x='time_delta_rounded',y='mag',data=quake_plot,ax=ax3,color='red',inner='quartile',split=True,bw=0.5,scale='width')
plt.savefig('custom_plot.png',dpi=300)
# plt.show()

#%%
#plot the magnitude on a log scale y-axis vs the frequency on the x-axis
quake_freq = quake.groupby(['mag_rounded'],as_index=False)['mag'].count()
quake_freq['count'] = quake_freq['mag']
plt.clf
plt.figure(figsize=(15,10))
plt.scatter(quake_freq['count'],quake_freq['mag_rounded'],color='cyan',s=50)
plt.yscale('log')
plt.xlabel('Count ("Frequency")',size=10)
plt.ylabel('Magnitude',size=10)
plt.savefig('plot_2.png')
# plt.show()
#%%
#plot the lat-long over time to understand if a clustering of aftershocks is moving over time
quake_plot.sort_values('time_delta',inplace=True)
plt.figure(figsize=(15,10))
img = plt.scatter(quake_plot['latitude'],quake_plot['longitude'],s=quake_plot['mag']*250-600,c = quake_plot['time_delta'],alpha=0.7)
plt.xlim((61.2,61.6))
plt.ylim((-150.2,-149.8))
plt.xlabel('longitude')
plt.ylabel('latitude')
plt.colorbar()
plt.savefig('location over time.png',dpi=300)
# plt.show()
